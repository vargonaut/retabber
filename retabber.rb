class Retabber
  class Checker
    def initialize file
      @file = file
    end

    def assessment

    end
  end

  def initialize base_directory, file_types
    @base_directory, @file_types = base_directory, file_types
  end

  def self.retab base_directory, file_types = ['.rb']
    file_types = Array(file_types)
    new(base_directory, Array(file_types)).retab
  end

  def retab
    intro
    file_list.each do |file|
      file_checker = Checker.new file
      case file_checker.assessment
      when :ugly
        log_mixed file
      when :bad
        log_bad file
        transform file
      when :good
        log_good file
      end
    end
    outro
  end

  private
  def intro
    puts "#{file_list.count} files found."
  end

  def file_list
    @file_list ||= Dir.glob(File.join(@base_directory, '**',  "*.rb"))
  end

  def file_type_regex
    "(#{@file_types.join('|')})"
  end

  def outro
    tell_about_the :good
    tell_about_the :bad
    tell_about_the :ugly
  end

  def tell_about_the type

  end
end


Retabber.retab '/Users/vargo/dev/whitespace', '.rb'
